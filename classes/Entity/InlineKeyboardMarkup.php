<?php

/**
 * Telegram Bot API 6.4
 */

namespace FSA\Telegram\Entity;

class InlineKeyboardMarkup extends AbstractEntity
{

    public array $inline_keyboard = [];
}
