<?php

/**
 * Telegram Bot API 6.4
 */

namespace FSA\Telegram\Entity;

class VideoChatEnded extends AbstractEntity
{

    public int $duration;
}
