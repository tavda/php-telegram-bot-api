<?php

/**
 * Telegram Bot API 6.4
 */

namespace FSA\Telegram\Entity;

class ForumTopicEdited extends AbstractEntity
{

    public ?string $name;
    public ?string $icon_custom_emoji_id;
}
