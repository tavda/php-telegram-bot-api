<?php

/**
 * Telegram Bot API 6.4
 */

namespace FSA\Telegram\Entity;

class ResponseParameters extends AbstractEntity
{

    public ?int $migrate_to_chat_id;
    public ?int $retry_after;
}
