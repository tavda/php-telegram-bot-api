<?php

/**
 * Telegram Bot API 6.4
 */

namespace FSA\Telegram\Entity;

class MessageAutoDeleteTimerChanged extends AbstractEntity
{

    public int $message_auto_delete_time;
}
