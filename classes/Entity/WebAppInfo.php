<?php

/**
 * Telegram Bot API 6.4
 */

namespace FSA\Telegram\Entity;

class WebAppInfo extends AbstractEntity
{

    public string $url;
}
