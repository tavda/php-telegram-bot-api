<?php

/**
 * Telegram Bot API 6.4
 */

namespace FSA\Telegram\Entity;

class WebAppData extends AbstractEntity
{

    public string $data;
    public string $button_text;
}
