<?php

/**
 * Telegram Bot API 6.4
 */

namespace FSA\Telegram\Entity;

class KeyboardButtonPollType extends AbstractEntity
{

    public ?string $type;
}
